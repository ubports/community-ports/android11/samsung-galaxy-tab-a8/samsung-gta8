#!/bin/bash
set -xe

export BSP_BUILD_FAMILY=sharkl5Pro
export BSP_BUILD_DT_OVERLAY=y
export BSP_BUILD_ANDROID_OS=y

# Fix ueventd.rc permissions
chmod 644 overlay/system/usr/share/halium-overlay/system/etc/ueventd.rc

[ -d build ] || git clone https://gitlab.com/ubports/community-ports/halium-generic-adaptation-build-tools build -b halium-11
./build/build.sh "$@"
